package org.leadium.report.allure

import io.qameta.allure.AllureLifecycle
import org.leadium.core.utils.createNewFile
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.*

/**
 * Method generates environment.properties file with environment info,
 * then writes it to allure-results directory.
 * @param allureResultsDir String
 */
fun AllureLifecycle.writeEnvironmentProperties(baseUrl: String, allureResultsDir: String) {
    checkAllureResultsDir(allureResultsDir)

    val environmentFile = File(allureResultsDir, "environment.properties")
    if (!environmentFile.exists()) {
        FileOutputStream(createNewFile(environmentFile), false).use { fos ->

            fun createProperties(): Properties {
                with(Properties()) {
                    setProperty("OS", System.getProperty("os.name"))
                    setProperty("Env", baseUrl)
                    setProperty("User", System.getProperty("user.name"))
                    return this
                }
            }

            createProperties().store(fos, null)
            fos.flush()
            fos.close()
        }
    }
}

/**
 * Method copies categories.json file from project resources to allure-results directory
 */
fun AllureLifecycle.copyCategories(categoriesJsonFile: File, allureResultsDir: String) {
    checkAllureResultsDir(allureResultsDir)
    val target = File("$allureResultsDir/categories.json")
    if (!target.exists())
        FileInputStream(categoriesJsonFile).channel
            .use { source ->
                FileOutputStream(target).channel
                    .use { destination -> destination.transferFrom(source, 0, source.size()) }
            }
}

/**
 * Tests whether the file or directory denoted by this abstract pathname
 * exists.
 *
 * If not exists - creates the directory named by this abstract pathname, including any
 * necessary but nonexistent parent directories.
 *
 * @throws  SecurityException
 *          If a security manager exists and its {@link
 *          java.lang.SecurityManager#checkRead(java.lang.String)}
 *          method denies read access to the file or directory
 */
private fun checkAllureResultsDir(allureResultsDir: String) {
    with(File(allureResultsDir)) {
        if (!exists()) mkdirs()
    }
}