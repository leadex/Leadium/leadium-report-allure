package org.leadium.report.allure.description


/**
 * Annotation that allows to attach a description for a test.
 */
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class Description(
    /**
     * Simple description text as String.
     *
     * @return Description text.
     */
    val value: String = ""
)