package org.leadium.report.allure.description

import io.qameta.allure.Allure
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.tuple.Pair
import org.leadium.core.TestCase
import org.leadium.core.utils.replaceCamelCaseWithUnderscores
import java.io.File
import java.nio.charset.StandardCharsets

/**
 * Adds descriptionHtml to current test. Takes no effect
 * if no test run at the moment. Note that description will take no effect if descriptionHtml is
 * specified.
 */
class DescriptionAppender(val rootProjectPath: String, val gitMasterUrl: String) {

    private val htmlBody = "<html><body>"
    private val bodyHtml = "</body></html>"

    private val descriptionBuilder = StringBuilder()

    /**
     * @suppress
     */
    fun appendPropertyFile(propertyFile: File?, separator: String = "="): DescriptionAppender {
        propertyFile?.let {
            appendLinkToFile(propertyFile, "Property File")
            val properties = IOUtils.toString(propertyFile.inputStream(), StandardCharsets.UTF_8)
            val propertiesArray = properties.split("\n").toTypedArray()
            val sb = StringBuilder("<h3>Test Data</h3>")
            sb.append("<div>")
            propertiesArray.forEach { v ->
                if (v.contains(separator))
                    sb
                        .append("<p>")
                        .append(v.split(separator)[0])
                        .append(": ")
                        .append("<strong>")
                        .append(v.split(separator)[1])
                        .append("</strong>")
                        .append("</p>")
                else
                    sb
                        .append("<p>")
                        .append(v)
                        .append("</p>")
            }
            sb.append("</div>")
            descriptionBuilder.append(sb).append("\n")
        }
        return this
    }

    /**
     * @suppress
     */
    fun appendTestDataMap(map: Map<String, Pair<String, String>>): DescriptionAppender {
        val sb = StringBuilder("<h3>Test Data</h3>")
        sb.append("<div>")
        map.forEach { (k, v) ->
            sb
                .append("<p>")
                .append(k)
                .append(": ")
                .append("<strong>")
                .append(v.left)
                .append("</strong>")
                .append("</p>")
        }
        sb.append("</div>")
        descriptionBuilder.append(sb).append("\n")
        return this
    }

    /**
     * @suppress
     */
    fun appendLinkToFile(file: File?, label: String = "Data File"): DescriptionAppender {
        file?.let {
            val sb = StringBuilder("\n")
                .append("<div>")
                .append("<strong>")
                .append("$label: ")
                .append("</strong>")
                .append("<a href=\"${getLinkToGit(file)}\">${file.nameWithoutExtension}</a>")
                .append("</div>")
            descriptionBuilder.append(sb).append("\n")
        }
        return this
    }

    fun appendLinkToGSheet(spreadsheetId: String): DescriptionAppender {
        val sb = StringBuilder("\n")
            .append("<div>")
            .append("<strong>")
            .append("Google Spreadsheet: ")
            .append("</strong>")
            .append("<a href=https://docs.google.com/spreadsheets/d/$spreadsheetId>$spreadsheetId</a>")
            .append("</div>")
        descriptionBuilder.append(sb).append("\n")
        return this
    }

    /**
     * @suppress
     */
    fun appendString(string: String): DescriptionAppender {
        descriptionBuilder.append(string).append("\n")
        return this
    }

    /**
     * @suppress
     */
    fun apply() {
        descriptionBuilder.append(bodyHtml)
        Allure.descriptionHtml(descriptionBuilder.toString())
    }

    /**
     * @suppress
     */
    fun appendTestCaseDescription(description: String): DescriptionAppender {
        val htmlText = stringToHtmlText(description)
        descriptionBuilder.append(htmlBody)
        val sb = StringBuilder()
            .append("<div style=\"background-color: #fefacd; border:10px solid #fefacd;\">")
            .append("<p>")
            .append(htmlText)
            .append("</p>")
            .append("</div>")
        descriptionBuilder.append(sb).append("\n")
        descriptionBuilder.append("<br>")
        return this
    }

    /**
     * @suppress
     */
    private fun stringToHtmlText(string: String?): String {
        val sb = StringBuilder()
        string?.trim()?.lines()?.forEach {
            sb.append(it).append("<br>")
        }
        return sb.toString()
    }

    /**
     * @suppress
     */
    fun appendLinkToTestCase(testCase: TestCase) : DescriptionAppender {
        val testCaseName = testCase::class.java.name
        val replacedTestCaseName = (testCaseName.replace(".", "/").substringBefore("$") + ".kt")
            .replace("/", System.getProperty("file.separator"))
            .replace("\\\\", "\\")
        val testCaseFile = File(System.getProperty("rootProjectPath"))
            .walkTopDown().find { file -> file.path.contains(replacedTestCaseName) }
        testCaseFile.let {
            val sb = StringBuilder("\n")
                .append("<div>")
                .append("<strong>")
                .append("Test Case Class: ")
                .append("</strong>")
                .append("<a href=\"${getLinkToGit(testCaseFile!!)}\">${testCaseFile.nameWithoutExtension}</a>")
                .append("</div>")
            descriptionBuilder.append(sb).append("\n")
        }
        return this
    }

    /**
     * @suppress
     */
    private fun getLinkToGit(file: File) = gitMasterUrl + file.path.substring(rootProjectPath.length, file.path.length)

    /**
     * @suppress
     */
    private fun generateDocUrl(testCase: TestCase): String {
        val sb = StringBuilder()
        testCase::class.java.canonicalName.replaceCamelCaseWithUnderscores()
            .split(".")
            .reversed()
            .forEach { s: String -> sb.append(s).append("_") }
        return "#_" + sb.toString().substring(0, sb.toString().length - 1).toLowerCase()
    }
}