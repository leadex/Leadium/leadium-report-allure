package org.leadium.report.allure.junit5

import io.qameta.allure.Allure
import org.json.JSONObject
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback
import org.junit.jupiter.api.extension.ExtensionContext
import java.io.BufferedWriter
import java.io.FileReader
import java.io.FileWriter
import java.util.*

class HistoryIdReplacer : BeforeTestExecutionCallback {

    /**
     * @suppress
     */
    override fun beforeTestExecution(context: ExtensionContext?) {
        testCases.add(Allure.getLifecycle().currentTestCase.get())
    }

    companion object {

        private var historyId: String? = null
        private var testCases = ArrayList<String>()

        fun updateResults(
            userDir: String,
            allureResultsDir: String
        ) {
            for (testCase in testCases)
                updateTestCaseStatusDetails(
                    userDir,
                    allureResultsDir,
                    testCase
                )
            testCases.clear()
        }

        private fun updateTestCaseStatusDetails(
            userDir: String,
            allureResultsDir: String,
            uuid: String
        ) {
            val path = "$userDir/$allureResultsDir/$uuid-result.json"
            val json = JSONObject(FileReader(path).readText())
            if (historyId.isNullOrEmpty())
                historyId = json.getString("historyId")
            json.put("historyId", historyId)
            val bufferedWriter = BufferedWriter(FileWriter(path))
            bufferedWriter.write(json.toString())
            bufferedWriter.close()
        }
    }
}