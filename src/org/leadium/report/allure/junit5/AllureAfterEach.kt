package org.leadium.report.allure.junit5

import com.assertthat.selenium_shutterbug.core.Capture
import com.assertthat.selenium_shutterbug.core.Shutterbug
import com.codeborne.selenide.WebDriverRunner
import io.qameta.allure.Allure
import io.qameta.allure.model.Status
import org.apache.log4j.Logger
import org.junit.jupiter.api.extension.AfterTestExecutionCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.leadium.report.AttachmentSyntax
import org.leadium.report.allure.copyCategories
import org.leadium.report.allure.writeEnvironmentProperties
import java.io.File
import java.util.*
import javax.imageio.ImageIO

/**
 * Junit5 callback extension for Allure called after test execution.
 * @param baseUrl String
 * @param categoriesJson File - copy from
 * @param allureResultsDir String - copy to
 */
class AllureAfterEach(val baseUrl: String, val categoriesJson: File, val allureResultsDir: String) :
    AfterTestExecutionCallback, AttachmentSyntax {

    private val log = Logger.getLogger(this.javaClass.simpleName)

    /**
     * @suppress
     */
    override fun afterTestExecution(context: ExtensionContext?) {
        log.info(this::afterTestExecution.name)

        Allure.getLifecycle().writeEnvironmentProperties(baseUrl, allureResultsDir)
        Allure.getLifecycle().copyCategories(categoriesJson, allureResultsDir)

        val failed = context!!.executionException.isPresent
        if (failed) {
            attachFullSizeScreenshot()
            appendAssertionFailedErrorStep(context)
        }
    }

    /**
     * @suppress
     */
    private fun appendAssertionFailedErrorStep(context: ExtensionContext?) {
        log.info(this::appendAssertionFailedErrorStep.name)

        val throwable = context!!.executionException.get()
        Allure.step(throwable.localizedMessage, Status.FAILED)
    }

    /**
     * @suppress
     */
    private fun attachFullSizeScreenshot() {
        log.info(this::attachFullSizeScreenshot.name)
        try {
            val wholePage = Shutterbug.shootPage(WebDriverRunner.getWebDriver(), Capture.FULL, true).image
            val outputFile = File("$allureResultsDir/${UUID.randomUUID()}-attachment.png")
            ImageIO.write(wholePage, "png", outputFile)
            attachPng("Screenshot", outputFile)
            outputFile.delete()
        } catch (e: IllegalStateException) {
            //
        }
    }
}