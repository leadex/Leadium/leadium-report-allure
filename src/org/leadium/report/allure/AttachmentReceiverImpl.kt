package org.leadium.report.allure

import io.qameta.allure.Allure
import org.leadium.report.AttachmentReceiver
import java.io.InputStream

class AttachmentReceiverImpl : AttachmentReceiver {

    override fun addAttachment(name: String, type: String, fileExtension: String, stream: InputStream) {
        Allure.getLifecycle().addAttachment(name, type, fileExtension, stream)
    }

    override fun addAttachment(name: String, type: String, fileExtension: String, body: ByteArray) {
        Allure.getLifecycle().addAttachment(name, type, fileExtension, body)
    }
}